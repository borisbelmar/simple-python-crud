from mysql.connector import connect

DB_NAME = 'users_python'
TABLE_NAME = 'users'
COLS = [
  '`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY',
  '`username` VARCHAR(45) NULL',
  '`email` VARCHAR(45) NULL',
  '`password` VARCHAR(45) NULL',
  '`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
  '`updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
]

CONFIG = {
  'host': 'localhost',
  'user': 'root',
  'password': 'root',
  'database': DB_NAME,
  'auth_plugin': 'mysql_native_password',
  'port': 9999
}

def get_string_cols(cols):
  string_col = ""
  for col in cols:
    if col != cols[-1]:
      string_col += f"{col}, "
    else:
      string_col += col
  return string_col

def get_connection():
  return connect(
    host = CONFIG['host'],
    user = CONFIG['user'],
    password = CONFIG['password'],
    database = CONFIG['database'],
    auth_plugin = CONFIG['auth_plugin'],
    port = CONFIG['port']
  )

def generate_db():
  connection = connect(
    host = CONFIG['host'],
    user = CONFIG['user'],
    password = CONFIG['password'],
    auth_plugin = CONFIG['auth_plugin'],
    port = CONFIG['port']
  )
  cursor = connection.cursor()
  cursor.execute(f'CREATE DATABASE IF NOT EXISTS {DB_NAME}')
  cursor.execute(f'USE {DB_NAME}')
  cursor.execute(f'CREATE TABLE IF NOT EXISTS {TABLE_NAME} ({get_string_cols(COLS)})')