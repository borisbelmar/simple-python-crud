from database import get_connection

def display_all_users():
  connection = get_connection()
  cursor = connection.cursor()
  cursor.execute("SELECT * FROM users;")
  records = cursor.fetchall()
  heading = f"Total registered users in the system: {cursor.rowcount}"

  print(heading)
  print("-" * len(heading))

  for row in records:
    print(f"ID: {row[0]}")
    print(f"Username: {row[1]}")
    print(f"Email: {row[2]}")
    print(f"Created At: {row[4]}\n")

  cursor.close()
  connection.close()
  print("MySQL connection is closed")

def search_users(query):
  connection = get_connection()
  sql_stmt = f"SELECT * FROM users WHERE username LIKE '%{query}%' OR email LIKE '%{query}%'"
  cursor = connection.cursor()
  cursor.execute(sql_stmt)
  records = cursor.fetchall()
  heading = f"search for '{query}' returned: {cursor.rowcount} rows"
  
  print(heading)
  print ("-" * len(heading))
  
  for row in records:
    print(f"ID: {row[0]}")
    print(f"Username: {row[1]}")
    print(f"Email: {row[2]}")
    print(f"Created At: {row[4]}\n")
  cursor.close()
  connection.close()
  print("MySQL connection is closed")

def add_new_user(user):
  connection = get_connection()
  sql_stmt = """INSERT INTO users (username,email,password) VALUES (%s,%s,%s)"""
  cursor = connection.cursor(prepared=True)
  cursor.execute(sql_stmt,user)
  connection.commit()
  cursor.close()
  connection.close()
  print("Record successfully inserted into the database using prepared stament")

def update_user(user):
  connection = get_connection()
  sql_stmt = "UPDATE users SET updated_at = NOW(), username = %s,email = %s,password = %s WHERE id = %s"
  cursor = connection.cursor(prepared=True)
  cursor.execute(sql_stmt,user)
  connection.commit()
  cursor.close()
  connection.close()
  print("Record successfully updated in the database using prepared stament")

def delete_user(id):
  connection = get_connection()
  sql_stmt = "DELETE FROM users WHERE id = %s"
  param = (id)
  cursor = connection.cursor(prepared=True)
  cursor.execute(sql_stmt,param)
  connection.commit()
  cursor.close()
  connection.close()
  print("Record successfully deleted from the database using prepared stament")



